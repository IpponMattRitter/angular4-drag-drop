import { Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class TypeCheckService {
  private dropTypeItem = new Subject<any>();
  constructor() { }
  public setDropTypeItem(item){
    this.dropTypeItem.next(item);
  }
   public getDropTypeItem(){
    return this.dropTypeItem.asObservable();
  }
}

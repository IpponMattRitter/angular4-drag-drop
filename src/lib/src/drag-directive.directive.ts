import { Directive, ElementRef, HostListener, Input, Output, EventEmitter, OnDestroy} from '@angular/core';
import { DragDropDirectiveService } from './drag-drop-directive.service';
import { Subscription } from 'rxjs/Subscription';

@Directive({
  selector: '[dragDirective]'
})
export class DragDirective implements OnDestroy{
  @Input('dragDirective')
  draggedItem:Object;
  @Input('dragHightlight')
  cssHighlight:string;
  @Output()
  releaseDrop:EventEmitter<any> = new EventEmitter();
  @Output()
  startDrag:EventEmitter<any> = new EventEmitter();
  
	private highlighted:boolean = false;
  private dropSubscription:Subscription;

  constructor(
    private el: ElementRef,
    private dragDropDirectiveService: DragDropDirectiveService
   ) {
  	this.el.nativeElement.draggable='true';
  }
  @HostListener('mouseenter') onMouseEnter() {
    this.highlight();
  }
  @HostListener('mouseleave') onMouseOut(){
  	this.highlight();
  }
  @HostListener('dragstart',['$event']) onDragStart(event:any){
    // html draggable will not transfer an object, so we stringify it
    let transferObject = { object: this.draggedItem, id: 'draggedItemID-' + new Date().getTime()}
    let transferObjectString = JSON.stringify(transferObject);
    event.dataTransfer.setData("text", transferObjectString);
    this.dragDropDirectiveService.setDragItem(this.draggedItem);
    this.dropSubscription = this.dragDropDirectiveService.getDropItem().subscribe(
      item => {
        this.emitDraggedItem(this.draggedItem);
      }
    );
    this.startDrag.emit(this.draggedItem);
  }
  @HostListener('dragend') onDragEnd(){
    if (typeof this.dropSubscription !== 'undefined'){
      this.dropSubscription.unsubscribe();
    }
  }

  ngOnDestroy(){
    if (typeof this.dropSubscription !== 'undefined') {
      this.dropSubscription.unsubscribe();
    }
  }

  private emitDraggedItem(item){
    this.releaseDrop.emit(item);
    this.dropSubscription.unsubscribe();
  }

  private highlight(){
    if (this.cssHighlight){
      if (!this.highlighted){
        this.el.nativeElement.classList.add(this.cssHighlight);
      } else {
        this.el.nativeElement.classList.remove(this.cssHighlight);
      }
    }
    this.highlighted = !this.highlighted;
  }
}
